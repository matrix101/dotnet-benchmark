﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using BenchmarkDotNet.Order;

namespace Benchmark
{
    [CsvMeasurementsExporter]
    [RPlotExporter]
    [MemoryDiagnoser]
    public class Benchmarks
    {
        private static readonly Samples Samples = new();

        [Benchmark]
        public int KidsCountWithLinq() => Samples.KidsCountWithLinq();
        [Benchmark]
        public int KidsWhereCountWithLinq() => Samples.KidsWhereCountWithLinq();
        [Benchmark]
        public int KidsCountForLoop() => Samples.KidsCountForLoop();
        [Benchmark]
        public int KidsCountForEachLoop() => Samples.KidsCountForEachLoop();
        [Benchmark]
        public int KidsCountLinqParallel() => Samples.KidsCountLinqParallel();
        [Benchmark]
        public int KidsCountParallel() => Samples.KidsCountParallel();
    }
}