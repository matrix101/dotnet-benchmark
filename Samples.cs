﻿using Bogus;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Benchmark
{
    public class Samples
    {
        private readonly Faker<Person> _faker = new();
        private readonly List<Person> _list;
        public Samples()
        {
            _list = _faker
                .RuleFor(p => p.Name, f => f.Name.FullName())
                .RuleFor(p => p.Age, f => f.Random.Int(0, 100))
                .Generate(50000);
        }

        public int KidsWhereCountWithLinq()
        {
            return _list.Where(x => x.Age < 15).Count();
        }

        public int KidsCountWithLinq()
        {
            return _list.Count(x => x.Age < 15);
        }

        public int KidsCountForEachLoop()
        {
            var count = 0;
            foreach (var person in _list)
            {
                if (person.Age < 15)
                    count++;
            }
            return count;
        }

        public int KidsCountForLoop()
        {
            var count = 0;
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].Age < 15)
                    count++;
            }
            return count;
        }

        public int KidsCountLinqParallel()
        {
            return _list.AsParallel().Where(x => x.Age < 15).Count();
        }

        public int KidsCountParallel()
        {
            int count = 0;
            Parallel.ForEach(_list, p =>
            {
                if (p.Age < 15)
                    Interlocked.Increment(ref count);
            });
            return count;
        }
    }
}
