﻿namespace Benchmark
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
